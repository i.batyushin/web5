/*jslint devel: true */
function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    r.innerHTML = f1[0].value * f2[0].value;
    return false;
}
window.addEventListener("DOMContentLoaded", function () {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button1");
    b.addEventListener("click", click1);
});